## Class Service

Service responsible for managing the class content

## Getting start

```shell
$ go run main.go
```

## Testing

You can run all tests:

```shell
$ just test
```

or mannually:

```shell
$ go test ./...
```

If you prefer run in verbose mode execute:

```shell
$ go test -coverprofile cover.out ./... -v
```

```shell
$ go test -coverprofile cover.out ./...
$ go tool cover -html=cover.out -o cover.html
```
