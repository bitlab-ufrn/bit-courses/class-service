run:
  go run main.go

test:
  go get -d -v
  go test -coverprofile coverage.out ./... -v

build:
  go build