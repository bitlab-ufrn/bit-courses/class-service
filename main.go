package main

import (
	"time"

	"github.com/gofiber/fiber/v2"
	log "github.com/sirupsen/logrus"
)

const version string = "1.0.0"
const serviceName string = "class-service"

type Root struct {
	App     string `json:"app"`
	Version string `json:"version"`
	Startup string `json:"startup"`
}

func setupLog() {
	log.SetFormatter(&log.TextFormatter{
		DisableColors: false,
		FullTimestamp: true,
	})
}

func main() {
	setupLog()
	log.Info("Starting ", serviceName, " at version ", version)
	app := fiber.New()

	app.Get("/", rootHandler)
	// v1 := app.Group("/v1")

	app.Listen(":3000")
}

var root = Root{
	App:     serviceName,
	Version: version,
	Startup: time.Now().UTC().String(),
}

func rootHandler(c *fiber.Ctx) error {
	c.Status(200).JSON(&root)
	return nil
}
